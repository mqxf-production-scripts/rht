% hllhcedms.cls
%
% Simple latex style to produce "HL-LHC quality templates compatible" documents.
%
%
% DISCLAIMER:
% I am not an expert in LaTeX, but I tried to make this template as "simple"
% as possible. Maybe it can be re-done better by the LaTeX experts if it will
% turn out to be useful for many.
% This work is based on examples taken from:
%  a) Minutes in Less Than Hours: Using LaTex Resources by Jim Hefferon
%     (ftpmaint@tug.ctan.org)
%       http://tutex.tug.org/pracjourn/2005-4/hefferon/hefferon.pdf
%     who suggests to follow the "The LaTex Companion":
%       http://www.latex-project.org/help/books/tlc2-ch0.pdf
%
%  b) "How to write a LaTeX class file and design your own CV (Part 1)":
%       https://it.sharelatex.com/blog/2011/03/27/how-to-write-a-latex-class-file-and-design-your-own-cv.html
%
%  c) many hints from google....
%
%  d) https://www.latex-project.org/help/documentation/clsguide.pdf
%
% V1.0: Jan 2017 - davide.gamba@cern.ch
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Class structure: identification part
% ---
\ProvidesClass{hllhcedms}[2017/01/20 version 1.00 of HL-LHC EDMS-like documents]
\NeedsTeXFormat{LaTeX2e}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Class structure: declaration of options part
% ---
% Just pass all options to the article class which is my standard environment.
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Class structure: execution of options part
% ---
\ProcessOptions \relax

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Class structure: declaration of options part
% ---
\LoadClass{article}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Define some internal variables used to define the geometry of the page
% --- Some might be changeable by the user... 
%     but they could destroy everything... be careful! 
% 
\newlength{\gleftspace}
\newlength{\grightspace}
\newlength{\gtopspace}
\newlength{\gbottomspace}
\newlength{\gtextmargin}
\newlength{\gthicklinewidth}
\newlength{\gthinlinewidth}
% here is where one can intervene to change some dimensions. 
\setlength{\gleftspace}{16mm}		% left margin
\setlength{\grightspace}{16mm}		% right margin
\setlength{\gtopspace}{34mm}		% space on top for logos & C.
\setlength{\gbottomspace}{21mm}		% bottom space  for footers
\setlength{\gtextmargin}{5mm}		% the normal text will need to have some additional margin not to hit the border this it is.
\setlength{\gthicklinewidth}{1.pt}	% thick of the border and in general of the lines in "maintable" environment 
\setlength{\gthinlinewidth}{0.6pt}	% one of block on top of the page has slightly thinner border. here it is.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Start defining commands/environment that people will use
% ---

%
% -- Define some commands for holding document informations
%    These will be the edms number, the revision, validity, reference number,
%    template number... In the same way one could add more!
\newcommand*{\edmsno}[1]{\def\eedmsno{#1}}
\newcommand*{\rev}[1]{\def\rrev{#1}}
\newcommand*{\validity}[1]{\def\vvalidity{#1}}
\newcommand*{\reference}[1]{\def\rreference{#1}}
\newcommand*{\templateedmsno}[1]{\def\ttemplateedmsno{#1}}


%
% -- some "standard" text style regularly used in mountable
% standard font sizes for 11pt standard font size.
%  \LARGE = 17.28pt -> used where docx uses 18pt
%  \Large =  14.4pt -> used where docx uses 14pt
\newcommand*{\maintabletitle}[1]{{\LARGE \textbf{#1}}}
\newcommand*{\maintablesubtitle}[1]{{\Large \textbf{#1}}}

%
% -- Environmente for adding simple text.
%    It adds some small indentation with respect to external frame.
\RequirePackage{changepage}
\RequirePackage[explicit]{titlesec}
\RequirePackage{caption}
\RequirePackage{subcaption}
\newenvironment{maincontent}%
  {\begin{adjustwidth}{\gtextmargin}{\gtextmargin}}%
  {\end{adjustwidth}}
%
\newenvironment{maincontentstyled}%
  {%
  \titleformat{\section}{\normalfont\large\bfseries}{\makebox[1cm][l]{\thesection}}{0pt}{\MakeUppercase{##1}}
  \titlespacing*{\section}{-1cm}{12pt}{10pt}
  \titleformat{\subsection}{\normalfont\large\bfseries}{\makebox[1cm][l]{\thesubsection}}{0pt}{##1}
  \titlespacing*{\subsection}{-1cm}{12pt}{12pt}
  \titleformat{\subsubsection}{\normalfont\large\bfseries}{\makebox[1cm][l]{\thesubsubsection}}{0pt}{##1}
  \titlespacing*{\subsubsection}{-1cm}{12pt}{12pt}
  \setlength{\parindent}{0pt}
  \setlength{\parskip}{2pt}%
  %
  \captionsetup{labelfont={bf},textfont={bf},belowskip={2.88pt},aboveskip={2.88pt}}
  %  One could actually specify different setups for different type of floats... figure, table, subfigure...
  %\captionsetup[figure]{labelfont={bf},textfont={bf},belowskip={0pt},aboveskip={2.88pt}}
  %
  \begin{adjustwidth}{ 1.5cm }{\gtextmargin}}%
  {\end{adjustwidth}}


%
% -- A command to create an H line over all the document
\newcommand*{\mainline}[1][\gthicklinewidth]{
\noindent\rule{\textwidth}{#1}
}

%
% -- some additional definitions for tabularx to create proper content within tables
\RequirePackage{tabularx}
\RequirePackage{booktabs}
\RequirePackage{setspace}
%
\newcolumntype{L}[1]{>{\hsize=#1\hsize\raggedright\arraybackslash}X}%
\newcolumntype{R}[1]{>{\hsize=#1\hsize\raggedleft\arraybackslash}X}%
\newcolumntype{C}[1]{>{\hsize=#1\hsize\centering\arraybackslash}X}%
%
% Definition of the "maintable" environment for 
%    The optional argument is the alignment in vertical of ALL! the cells. It can be p (=top);  m(=centre); b(=bottom).
%    Following one has to specify the format of the table columns (is going to be a tabularx wide as the whole page!). 
%       Examples are L[<width>]; R[<width>]; C[<width>] for left, right, centre horizontal alignment.
%         The parameter <width> is used for defining the width in a "relative" fashion.
%         The sum of all widths has to be equal to the number of columns you are defining.
\newenvironment{maintable}[1][p]
  {
    \renewcommand{\tabularxcolumn}[1]{#1{##1}}
    \setlength\arrayrulewidth{\gthicklinewidth}
    \renewcommand{\arraystretch}{1.6}  % here I enlarge the space between the text and the table borders
    \noindent\tabularx{\textwidth}
  }
  {
    \endtabularx
  }


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Page layout: 
% ---
%
% -- Force actual content to be within the rounded corner box and for the positioning of the headers
\RequirePackage[
  left=\gleftspace, right=\grightspace,
  top=\gtopspace, bottom=\gbottomspace,
  headheight=22mm, headsep=6mm,
  footskip=8mm
]{geometry}
%
% -- Now can start better defining the headers with logos and so on.
\RequirePackage{tikz}
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}
\RequirePackage{array}
\usetikzlibrary{positioning,calc}
%
\fancypagestyle{hilumiedms}{%
  \fancyhf{} % clear all six fields 
  \renewcommand{\headrulewidth}{0pt} % no head rule 
  \renewcommand{\footrulewidth}{0pt} % no foot rule
  % Add LHC and HL-LHC logos on top left of each page 
  \lhead{ 
    \hspace{5mm}
    \includegraphics[height=2cm]{CERNLogo.eps}
    \includegraphics[height=2cm]{HiLumiLogo.eps}
  }
  % Add reference numbers on top right header
  \rhead{
    \begin{tikzpicture}[overlay,remember picture]
        \node (refRect)  [rectangle, draw, text width=68mm, minimum height=8mm,
                line width=\gthinlinewidth, rounded corners=7pt, black, anchor=south east, align=left,
                font=\fontsize{10}{12}\selectfont]
            at (0mm,-1mm) {\hspace{3mm}\textbf{REFERENCE :} \rreference};
        \node (edmsRect) [rectangle, draw, text width=68mm, minimum height=10mm,
                line width=\gthicklinewidth, rounded corners=7pt, black, anchor=south east, align=center,
                font=\fontsize{10}{12}\selectfont]
           at (refRect.north east) [yshift=1pt] {\begin{tabular}{
                >{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{20mm} | %
                >{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{12mm} | %
                >{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{20mm}}
                    EDMS NO. 	& REV. 	& VALIDITY \\
                    \textbf{\eedmsno}	& \textbf{\rrev}	& \textbf{\vvalidity}
                \end{tabular}};
    \end{tikzpicture} 
  }
  % add the page frame within the head
  \fancyhead[CO]{
  \begin{tikzpicture}[remember picture, overlay, >=latex] 
        \draw [line width=\gthicklinewidth,rounded corners=4mm, black]
            ($ (current page.north west) + (\gleftspace,-\gtopspace+4mm) $) 
            rectangle
            ($ (current page.south east) + (-\grightspace,\gbottomspace-4mm)   $); 
    \end{tikzpicture}
  }
  % Add page numbers on bottom left 
  \fancyfoot[LO]{
   \hspace{5mm}Page \thepage\ of \pageref{LastPage}
  }
  % Add template on bottom right
  \fancyfoot[RO]{
    \@ifundefined{ttemplateedmsno}{}
      {
        Template EDMS No.: \ttemplateedmsno\hspace{5mm}
      }
  }
  % on twoside documents we need to split between even(left) and odd(right) pages
  \if@twoside 
    \fancyhead[CE]{
    \begin{tikzpicture}[remember picture, overlay, >=latex] 
          \draw [line width=\gthicklinewidth,rounded corners=4mm, black]
              ($ (current page.north west) + (\grightspace,-\gtopspace+4mm) $) 
              rectangle
              ($ (current page.south east) + (-\gleftspace,\gbottomspace-4mm)   $); 
      \end{tikzpicture}
    }
    \fancyfoot[RE]{
       Page \thepage\ of \pageref{LastPage}\hspace{5mm}
    }
    \fancyfoot[LE]{
      \@ifundefined{ttemplateedmsno}{}
        {
          \hspace{5mm}Template EDMS No.: \ttemplateedmsno
        }
    }
  \fi
}
% set hilumiedms as standard page style
\AtBeginDocument{\pagestyle{hilumiedms}}
% Force to use the same page style also for a title pages:
\g@addto@macro{\maketitle}{\thispagestyle{hilumiedms}}
