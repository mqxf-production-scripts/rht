import pytimber, math, datetime
from scipy.interpolate import interp1d
import time
import pandas as pd
import numpy as np
import os

CSVvars = [
        'PV +B3.1-TC-N-3.1 [°C]',
        'Wsp zone +B3 [°C]',
        'OP zone +B3 [%]',
        'PV +B1.1-TC-N-1.1 [°C]',
        'Wsp zone +B1 [°C]',
        'OP zone +B1 [%]',
        'PV +B2.1-TC-N-2.1 [°C]',
        'Wsp zone +B2 [°C]',
        'OP zone +B2 [%]',
        'PV +B4.1-TC-N-4.1 [°C]',
        'Wsp zone +B4 [°C]',
        'OP zone +B4 [%]',
        'PV +B5.1-TC-N-5.1 [°C]',
        'Wsp zone +B5 [°C]',
        'OP zone +B5 [%]',
        'PV +B6.1-TC-N-6.1 [°C]',
        'Wsp zone +B6 [°C]',
        'OP zone +B6 [%]',
        'PV +B7.1-TC-N-7.1 [°C]',
        'Wsp zone +B7 [°C]',
        'OP zone +B7 [%]',
        'PV +B8.1-TC-N-8.1 [°C]',
        'Wsp zone +B8 [°C]',
        'OP zone +B8 [%]',
        'PV +A3.1-TC-N-3.1 [°C]',
        'Wsp zone +A3 [°C]',
        'OP zone +A3 [%]',
        'PV +A1.1-TC-N-1.1 [°C]',
        'Wsp zone +A1 [°C]',
        'OP zone +A1 [%]',
        'PV +A2.1-TC-N-2.1 [°C]',
        'Wsp zone +A2 [°C]',
        'OP zone +A2 [%]',
        'PV +A4.1-TC-N-4.1 [°C]',
        'Wsp zone +A4 [°C]',
        'OP zone +A4 [%]',
        'PV +A5.1-TC-N-5.1 [°C]',
        'Wsp zone +A5 [°C]',
        'OP zone +A5 [%]',
        'PV +A6.1-TC-N-6.1 [°C]',
        'Wsp zone +A6 [°C]',
        'OP zone +A6 [%]',
        'PV +A7.1-TC-N-7.1 [°C]',
        'Wsp zone +A7 [°C]',
        'OP zone +A7 [%]',
        'PV +A8.1-TC-N-8.1 [°C]',
        'Wsp zone +A8 [°C]',
        'OP zone +A8 [%]',
        'PV +GP1.0 MFC-1.0 Ar retort[l/h]',
        'Wsp +GP1.0 MFC-1.0 Ar retort[l/h]',
'PV +GP1.0 MFC-1.1 Ar tooling[l/h]',
    'Wsp +GP1.0 MFC-1.1 Ar tooling[l/h]',
    'PV +MT2.0 PS-1.0 p relative [mbar]',
    'PV +MT2.0 PS-2.0 p absolute [mbar]',
    'PV +MT2.0 PS-3.0 p vacuum [mbar]',
    'PV +MT2.0 GS-1.0 O2 standard [ppm]',
    'PV +MT2.0 GS-1.0 O2 tooling [ppm]',
    'PV +MT2.0 GS-1.0 O2 ref. gas [ppm]',
    'PV +GO1.0 GS-1.0 O2 [ppm]',
    'PV +MT3.0 TC-N-1.0 [°C]',
    'PV +MT3.0 TC-N-2.0 [°C]',
    'PV +MT3.0 TC-N-3.0 [°C]',
    'PV +MT3.0 TC-N-4.0 [°C]',
    'PV +MT3.0 TC-N-5.0 [°C]',
    'PV +MT3.0 TC-N-6.0 [°C]',
    'PV +MT3.0 TC-N-7.0 [°C]',
    'PV +MT3.0 TC-N-8.0 [°C]',
    'PV +MT3.0 TC-N-9.0 [°C]',
    'PV +MT3.0 TC-N-10.0 [°C]',
    'PV +MT3.0 TC-N-11.0 [°C]',
    'PV +MT3.0 TC-N-12.0 [°C]',
    'PV +MT3.0 TC-N-13.0 [°C]',
    'PV +MT3.0 TC-N-14.0 [°C]',
    'PV +MT3.0 TC-N-15.0 [°C]',
    'PV +MT3.0 TC-N-16.0 [°C]',
    'PV +MT3.0 TC-N-17.0 [°C]',
    'PV +MT3.0 TC-N-18.0 [°C]',
    'PV +MT3.0 TC-N-19.0 [°C]',
    'PV +MT3.0 TC-N-20.0 [°C]',
    'PV +MT3.0 TC-N-21.0 [°C]',
    'PV +MT3.0 TC-N-22.0 [°C]',
    'PV +MT3.0 TC-N-23.0 [°C]',
    'PV +MT3.0 TC-N-24.0 [°C]',
    'PV +MT3.0 TC-N-25.0 [°C]',
    'PV +MT3.0 TC-N-26.0 [°C]',
    'PV +MT3.0 TC-N-27.0 [°C]',
    'PV +MT3.0 TC-N-28.0 [°C]',
    'PV +MT3.0 TC-N-29.0 [°C]',
    'PV +MT3.0 TC-N-30.0 [°C]',
    'PV +MT3.0 TC-N-31.0 [°C]',
    'PV +MT3.0 TC-N-32.0 [°C]',
    'PV +MT3.0 TC-N-33.0 [°C]',
    'PV +MT3.0 TC-N-34.0 [°C]',
    'PV +MT3.0 TC-N-35.0 [°C]',
    'PV +MT3.0 TC-N-36.0 [°C]',
    'PV +MT3.0 TC-N-37.0 [°C]',
    'PV +MT3.0 TC-N-38.0 [°C]',
    'PV +MT3.0 TC-N-39.0 [°C]',
    'PV +MT3.0 TC-N-40.0 [°C]',
    'PV Delta temp',
    'test1',
    'test2',
    'test3',
    'test4',
    'test5',
    'test6',
    'test7',
    'test8',
    'test9',
    'test10',
    'test11',
    'test12',
    'test13',
    'test14',
    'test15',
    'test16',
    'test17',
    'test18',
    'test19',
    'test20',
    'test21',
    'test22',
    'test23',
    'test24',
    'test25',
    'test26',
    'test27',
    'test28',
    'test29',
    'operation message',
    'alarm']

Timbervars = [
        'LMFGLO3_HZ_B3_TT.POSST',
        'LMFGLO3_HZ_B3_T_WSP.POSST',
        'LMFGLO3_HZ_B3_EH.POSST',
        'LMFGLO3_HZ_B1_TT.POSST',
        'LMFGLO3_HZ_B1_T_WSP.POSST',
        'LMFGLO3_HZ_B1_EH.POSST',
        'LMFGLO3_HZ_B2_TT.POSST',
        'LMFGLO3_HZ_B2_T_WSP.POSST',
        'LMFGLO3_HZ_B2_EH.POSST',
        'LMFGLO3_HZ_B4_TT.POSST',
        'LMFGLO3_HZ_B4_T_WSP.POSST',
        'LMFGLO3_HZ_B4_EH.POSST',
        'LMFGLO3_HZ_B5_TT.POSST',
        'LMFGLO3_HZ_B5_T_WSP.POSST',
        'LMFGLO3_HZ_B5_EH.POSST',
        'LMFGLO3_HZ_B6_TT.POSST',
        'LMFGLO3_HZ_B6_T_WSP.POSST',
        'LMFGLO3_HZ_B6_EH.POSST',
        'LMFGLO3_HZ_B7_TT.POSST',
        'LMFGLO3_HZ_B7_T_WSP.POSST',
        'LMFGLO3_HZ_B7_EH.POSST',
        'LMFGLO3_HZ_B8_TT.POSST',
        'LMFGLO3_HZ_B8_T_WSP.POSST',
        'LMFGLO3_HZ_B8_EH.POSST',
        'LMFGLO3_HZ_A3_TT.POSST',
        'LMFGLO3_HZ_A3_T_WSP.POSST',
        'LMFGLO3_HZ_A3_EH.POSST',
        'LMFGLO3_HZ_A1_TT.POSST',
        'LMFGLO3_HZ_A1_T_WSP.POSST',
        'LMFGLO3_HZ_A1_EH.POSST',
        'LMFGLO3_HZ_A2_TT.POSST',
        'LMFGLO3_HZ_A2_T_WSP.POSST',
        'LMFGLO3_HZ_A2_EH.POSST',
        'LMFGLO3_HZ_A4_TT.POSST',
        'LMFGLO3_HZ_A4_T_WSP.POSST',
        'LMFGLO3_HZ_A4_EH.POSST',
        'LMFGLO3_HZ_A5_TT.POSST',
        'LMFGLO3_HZ_A5_T_WSP.POSST',
        'LMFGLO3_HZ_A5_EH.POSST',
        'LMFGLO3_HZ_A6_TT.POSST',
        'LMFGLO3_HZ_A6_T_WSP.POSST',
        'LMFGLO3_HZ_A6_EH.POSST',
        'LMFGLO3_HZ_A7_TT.POSST',
        'LMFGLO3_HZ_A7_T_WSP.POSST',
        'LMFGLO3_HZ_A7_EH.POSST',
        'LMFGLO3_HZ_A8_TT.POSST',
        'LMFGLO3_HZ_A8_T_WSP.POSST',
        'LMFGLO3_HZ_A8_EH.POSST',
        'LMFGLO3_RETORT_AR_FCV_FT.POSST',
        'LMFGLO3_RETORT_AR_FCV_WSP.POSST',
'LMFGLO3_TOOLING_AR_FCV_FT.POSST',
    'LMFGLO3_TOOLING_AR_FCV_WSP.POSST',
    'LMFGLO3_RETORT_PRESSURE_REL_PT.POSST',
    'LMFGLO3_RETORT_PRESSURE_ABS_PT.POSST',
    'LMFGLO3_RETORT_PRESSURE_VAC_PT.POSST',
    'LMFGLO3_STANDARD_O2T.POSST',
    'LMFGLO3_TOOLING_O2T.POSST',
    'LMFGLO3_REFERENCE_O2T.POSST',
    'LMFGLO3_RETORT_O2T.POSST',
    'LMFGLO3_TOOLING_TT01.POSST',
    'LMFGLO3_TOOLING_TT02.POSST',
    'LMFGLO3_TOOLING_TT03.POSST',
    'LMFGLO3_TOOLING_TT04.POSST',
    'LMFGLO3_TOOLING_TT05.POSST',
    'LMFGLO3_TOOLING_TT06.POSST',
    'LMFGLO3_TOOLING_TT07.POSST',
    'LMFGLO3_TOOLING_TT08.POSST',
    'LMFGLO3_TOOLING_TT09.POSST',
    'LMFGLO3_TOOLING_TT10.POSST',
    'LMFGLO3_TOOLING_TT11.POSST',
    'LMFGLO3_TOOLING_TT12.POSST',
    'LMFGLO3_TOOLING_TT13.POSST',
    'LMFGLO3_TOOLING_TT14.POSST',
    'LMFGLO3_TOOLING_TT15.POSST',
    'LMFGLO3_TOOLING_TT16.POSST',
    'LMFGLO3_TOOLING_TT17.POSST',
    'LMFGLO3_TOOLING_TT18.POSST',
    'LMFGLO3_TOOLING_TT19.POSST',
    'LMFGLO3_TOOLING_TT20.POSST',
    'LMFGLO3_TOOLING_TT21.POSST',
    'LMFGLO3_TOOLING_TT22.POSST',
    'LMFGLO3_TOOLING_TT23.POSST',
    'LMFGLO3_TOOLING_TT24.POSST',
    'LMFGLO3_TOOLING_TT25.POSST',
    'LMFGLO3_TOOLING_TT26.POSST',
    'LMFGLO3_TOOLING_TT27.POSST',
    'LMFGLO3_TOOLING_TT28.POSST',
    'LMFGLO3_TOOLING_TT29.POSST',
    'LMFGLO3_TOOLING_TT30.POSST',
    'LMFGLO3_TOOLING_TT31.POSST',
    'LMFGLO3_TOOLING_TT32.POSST',
    'LMFGLO3_TOOLING_TT33.POSST',
    'LMFGLO3_TOOLING_TT34.POSST',
    'LMFGLO3_TOOLING_TT35.POSST',
    'LMFGLO3_TOOLING_TT36.POSST',
    'LMFGLO3_TOOLING_TT37.POSST',
    'LMFGLO3_TOOLING_TT38.POSST',
    'LMFGLO3_TOOLING_TT39.POSST',
    'LMFGLO3_TOOLING_TT40.POSST',
    'PV Delta temp',
    'test1',
    'test2',
    'test3',
    'test4',
    'test5',
    'test6',
    'test7',
    'test8',
    'test9',
    'test10',
    'test11',
    'test12',
    'test13',
    'test14',
    'test15',
    'test16',
    'test17',
    'test18',
    'test19',
    'test20',
    'test21',
    'test22',
    'test23',
    'test24',
    'test25',
    'test26',
    'test27',
    'test28',
    'test29',
    'operation message',
    'alarm']

def get_timber_csv_map():
    timber_csv_map = {}
    for i,CSVvar in enumerate(CSVvars):
        timber_csv_map[Timbervars[i]] = CSVvar

    return timber_csv_map

def get_csv_timber_map():
    csv_timber_map = {}
    for i,Timbervar in enumerate(Timbervars):
        csv_timber_map[CSVvars[i]] = Timbervar

    return csv_timber_map

def timber_var_interpolators(timber_data):
    interpolators = {}
    timber_csv_map = get_timber_csv_map()
    keys = Timbervars
    for i, key in enumerate(keys):
        csvkey = timber_csv_map[key]
        interpolators[csvkey] = {}
        try:
            interpolators[csvkey]['interp'] = interp1d(timber_data[key][0],timber_data[key][1])
            interpolators[csvkey]['lim'] = np.min(timber_data[key][0]), np.max(timber_data[key][0])
        except:
            interpolators[csvkey] = None

    return interpolators

def create_timber_dataframe(interpolators, t1, t2, timestep=60): 
    t2 = t2 - t2 % timestep
    ran = np.arange(np.ceil(t1),np.floor(t2), timestep)
    df = {}
    df['time utc'] = pd.date_range(datetime.datetime.fromtimestamp(t1),datetime.datetime.fromtimestamp(t2),freq='T')
    df['date'] = df['time utc'].strftime('%d.%m.%Y')
    df['time'] = df['time utc'].strftime('%H:%M:%S')
    df['step'] = np.empty(np.shape(df['time utc'])).astype(int)
    df['step'][:] = 1 
    df['i'] = np.empty(np.shape(df['time utc'])).astype(int)
    df['i'][:] = 8
    for key in interpolators:
        if interpolators[key] != None:
            lim = interpolators[key]['lim']
            interp = interpolators[key]['interp']
            t01 = t1 
            t02 = lim[0] - (lim[0] - t1) % timestep + timestep
            t03 = lim[1] - (lim[1] - t02) % timestep 
            t04 = t2
            ran0 = np.arange(t01, t02, timestep)
            ran1 = np.arange(t02, t03, timestep)
            ran2 = np.arange(t03, t04, timestep)
            y0 = np.empty(np.shape(ran0))
            y0[:] = np.nan
            y2 = np.empty(np.shape(ran2))
            y2[:] = np.nan
            df[key] = np.concatenate((y0, interp(ran1), y2))
        else:
            print(key)
            df[key] = np.empty(np.shape(ran))
        if np.size(df[key]) != np.size(ran): print(("wrong size in ", key))
        sizes = [np.size(df[key]) for key in df]
    print (sizes)

    return pd.DataFrame(df)

def fetch(s1, s2):
    CSVvariables = CSVvars
    csv_timber_map = get_csv_timber_map()

    timber_variables = [csv_timber_map[var] for var in CSVvariables]

    ldb = pytimber.LoggingDB()

    #CSVvariables = ['PV +B3.1-TC-N-3.1 [°C]', 'PV +B5.1-TC-N-5.1 [°C]', 'PV +MT3.0 TC-N-8.0 [°C]']
    t1 = time.mktime(datetime.datetime.strptime(s1, '%Y-%m-%d %H:%M:%S').timetuple())
    t2 = time.mktime(datetime.datetime.strptime(s2, '%Y-%m-%d %H:%M:%S').timetuple())

    timber_data = ldb.get(timber_variables, t1, t2)
    interpolators = timber_var_interpolators(timber_data)
    df = create_timber_dataframe(interpolators, t1, t2)

    df[['i', 'date', 'time', 'step'] + CSVvars].to_csv('CR112_timber.csv', sep=';', decimal=',', index = False)

if __name__ == '__main__':
    fetch('2020-01-04 14:40:43', '2020-01-16 10:00:54')
