# RHT

Analyse MQXFB RHT cycles

INSTRUCTIONS:

*) The .csv to be analyzed is just copied from the oven's computer. Usually renamed using the coil numbering.

KNOWN SOLVED PROBLEMS (Eelis):

*) The following files (added after some trials) must be included with the RHT program:

 - hllhcedms_template_short.tex
 - hllhcedms.cls 
 - default.mapping
 - CERNLogo.eps
 - HiLumiLogo.eps

*) The textemplator needs to be the latest one. Otherwise the arguments do not correspond.

*) For the installation of the pdflatex utility, Miktex can be installed normally in the Windows machine. A syspath.insert line is added below the existing one: sys.path.insert(0, "C:/Users/jferrada/AppData/Local/Programs/MiKTeX/miktex/bin/x64/")
 
